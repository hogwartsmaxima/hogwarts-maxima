# Hogwarts Maxima #

Hogwarts Maxima is a free Harry Potter game.

### Can I help? ###

Yes, you can help. We are looking for developers.

### What are the developer requirements? ###

Developers should have an efficient knowledge of web development (specifically canvas games) and Javascript. A knowledge
of MySQL and PHP will also be very useful.

### Contact ###
To ask questions or to apply for dev, please email ryanfwu3@gmail.com